all: project4

project4: project4.o
	g++ project4.o -o project4.out -lrt -lpthread -std=c++0x

project4.o:
	g++ -O3 -c project4.cpp -lpthread -std=c++0x

clean:
	rm -rf *.o
	rm -rf *.cpp~
