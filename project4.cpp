#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <time.h>

#define OPERATIONS 			10000000
#define WAITING				1000

#define READ				0
#define WRITE				1
#define READ_WRITE			2
#define ATOMIC_READ_WRITE	3

#define WEAK				0
#define STRONG				1

using namespace std;

int NUM_THREADS;

int var;
atomic<int> a_var;

atomic<int> counter;

void read_thread(int type, int scaling) {

	// Getting thread on to different cores
	for(int i = 0; i < WAITING; ++i) {
		int x = i + 500;	
	}
	
	int x;
	// Check scaling
	int ops;
	if(scaling == 0)
		ops = OPERATIONS;
	else
		ops = OPERATIONS / NUM_THREADS;

	++counter;

	// Waiting for main thread to give the go ahead
	while(counter < NUM_THREADS + 1);

	// If reading
	if(type == READ) {
		for(int i = 0; i < ops; ++i) {
			x = var;
		}
	}

	// If Writing
	else if(type == WRITE) {
		for(int i = 0; i < ops; ++i) {
			var = i;
		}
	}

	// If read and writing
	else if(type == READ_WRITE) {
		for(int i = 0; i < ops; ++i) {
			x = var;
			var = x + i;
		}
	}

	// If doing atomic reads and writes
	else {
		for(int i = 0; i < ops; ++i) {
			x = a_var;
			a_var = x + i;
		}	
	}
}


void do_run(int type, int scaling) {
	
	if(type >= 4 || scaling >= 2) {
		fprintf(stderr, "Type or scaling too high. Try again.\n");
		exit(-1);
	} 

	var = 0;
	a_var = 0;
	counter = 0;
	struct timespec begin, end;
	vector<thread> threads;

	// Create threads
	for(int i = 0; i < NUM_THREADS; ++i) 
		threads.emplace_back(read_thread, type, scaling);

	// Wait for threads to be on different cores
	while(counter < NUM_THREADS);

	// begin counting
	clock_gettime(CLOCK_MONOTONIC, &begin);

	// Starts threads
	counter++;

	for(auto& th: threads)
		th.join();

	// After all threads are done get the time
	clock_gettime(CLOCK_MONOTONIC, &end);

	double accum = (end.tv_sec - begin.tv_sec) + (end.tv_nsec - begin.tv_nsec) / 1E9;

	if(type == READ)
		cout << "Read only ";
	else if(type == WRITE)
		cout << "Write only ";
	else if(type == READ_WRITE)
		cout << "Read and write ";
	else 
		cout << "Atomic Read and Write ";
	cout << "threads = " << accum << " seconds using ";

	if(scaling == WEAK)
		cout << "WEAK ";
	else
		cout << "STRONG ";

	cout << "scaling\n";

}

int scanargs() {
	cout << "Enter number of threads to be run: ";
	int threads;
	cin >> threads;
	return threads;
}



int main(int argc, char **argv) {
	NUM_THREADS = scanargs();
	cout << "\n----------------------------------------\n";

	do_run(READ_WRITE, WEAK);
	do_run(WRITE, WEAK);
	do_run(ATOMIC_READ_WRITE, WEAK);
	do_run(READ, WEAK);

	cout << "\n----------------------------------------\n";
	for(int i = 0; i < 4; ++i)
		do_run(i, STRONG);
	
}
